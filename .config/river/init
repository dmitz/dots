#!/bin/sh

# Background and border color
riverctl background-color 0x151515
riverctl border-color-focused 0x005577
riverctl border-color-unfocused 0x262626

# General binds
riverctl map normal Super Return spawn $TERMINAL

riverctl map normal Super B spawn "$TERMINAL -T float bluetuith --no-warning"
riverctl map normal Super+Shift B spawn "echo 'all toggle-visibility' > $XDG_RUNTIME_DIR/sandbar"

riverctl map normal Super C spawn "$TERMINAL pipe-viewer"

riverctl map normal Super D spawn passmenu
riverctl map normal Super+Shift D spawn "tofi-drun --drun-launch=true"

riverctl map normal Super V spawn "$TERMINAL $EDITOR"
riverctl map normal Super E spawn "$TERMINAL aerc"

riverctl map normal Super F toggle-fullscreen
riverctl map normal Super+Shift F toggle-float

riverctl map normal Super M spawn "$TERMINAL -T float ncmpcpp"
riverctl map normal Super+Shift M spawn "$TERMINAL -T float pulsemixer"

riverctl map normal Super N spawn "$TERMINAL newsboat"
riverctl map normal Super+Shift N spawn "notify-send 'Refreshing feeds...' && newsboat -x reload"

riverctl map normal Super Q close
riverctl map normal Super+Shift Q spawn "sysact rest"

riverctl map normal Super R spawn "$TERMINAL vifm"
riverctl map normal Super+Shift R spawn "$TERMINAL btm"

riverctl map normal Super T spawn "torwrap open"
riverctl map normal Super+Shift T spawn "torwrap toggle"

riverctl map normal Super W spawn qutebrowser
riverctl map normal Super+Shift W spawn "qutebrowser --temp-basedir -s content.private_browsing true"

riverctl map normal Super X spawn "wlopm --toggle '*'"
riverctl map normal Super+Shift X spawn "sysact shut"

riverctl map normal Super+Shift BackSpace spawn "sysact reb"

# Volume and MPD
for mode in normal locked
do
	riverctl map         $mode None XF86AudioMute        spawn "wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
	riverctl map         $mode Super BackSpace           spawn "wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
	riverctl map -repeat $mode None XF86AudioLowerVolume spawn "wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"
	riverctl map -repeat $mode None XF86AudioRaiseVolume spawn "wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+"
	riverctl map -repeat $mode Super Equal               spawn "wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+"
	riverctl map -repeat $mode Super Minus               spawn "wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"
	riverctl map         $mode Super Comma               spawn "mpc prev"
	riverctl map         $mode Super P                   spawn "mpc toggle"
	riverctl map         $mode Super Period              spawn "mpc next"
	riverctl map         $mode Super+Shift Comma         spawn "mpc seek 0%"
	riverctl map         $mode Super+Shift Period        spawn "mpc repeat"
done

# Screenshots
riverctl map normal None Print spawn "grim pic-full-$(date '+%y%m%d-%H%M-%S').png"
riverctl map normal Shift Print spawn "grimpick"

# Nodes/tags manipulation
for i in $(seq 1 9)
do
    tags=$((1 << ($i - 1)))
    riverctl map normal Super $i set-focused-tags $tags
    riverctl map normal Super+Shift $i set-view-tags $tags
done

riverctl map normal Control+Alt H snap left
riverctl map normal Control+Alt J snap down
riverctl map normal Control+Alt K snap up
riverctl map normal Control+Alt L snap right
riverctl map normal Super J focus-view next
riverctl map normal Super K focus-view previous
riverctl map normal Super Space zoom
riverctl map normal Super Tab focus-previous-tags
riverctl map normal Super BackSlash focus-previous-tags
riverctl map normal Super+Shift J swap next
riverctl map normal Super+Shift K swap previous

# Sticky
all_tags=$(((1 << 32) - 1))
sticky_tag=$((1 << 31))
all_but_sticky_tag=$(( $all_tags ^ $sticky_tag ))

riverctl map normal Super S toggle-view-tags $sticky_tag
riverctl spawn-tagmask $all_but_sticky_tag

for i in $(seq 1 9)
do
    tags=$((1 << ($i - 1)))
    riverctl map normal Super $i set-focused-tags $(($sticky_tag + $tags))
done

# Resizing
riverctl map normal Super H send-layout-cmd rivertile "main-ratio -0.05"
riverctl map normal Super L send-layout-cmd rivertile "main-ratio +0.05"
riverctl map normal Alt Right resize horizontal 100
riverctl map normal Alt Left resize horizontal -100
riverctl map normal Alt Up resize vertical 100
riverctl map normal Alt Down resize vertical -100

# Layout orientation
riverctl map normal Super Up    send-layout-cmd rivertile "main-location top"
riverctl map normal Super Right send-layout-cmd rivertile "main-location right"
riverctl map normal Super Down  send-layout-cmd rivertile "main-location bottom"
riverctl map normal Super Left  send-layout-cmd rivertile "main-location left"

# Input
pad=$(riverctl list-inputs | grep -i touchpad)
riverctl input $pad accel-profile adaptive
riverctl input $pad disable-while-typing enabled
riverctl input $pad disabled-on-external-mouse enabled
riverctl input $pad tap enabled
riverctl input $pad natural-scroll disabled

riverctl xcursor-theme Adwaita
riverctl focus-follows-cursor normal
riverctl hide-cursor timeout 5000
riverctl hide-cursor when-typing enabled

riverctl keyboard-layout -options "grp:ctrl_shift_toggle,altwin:menu_win" "us,ua"
riverctl set-repeat 50 300

riverctl map-pointer normal Super BTN_LEFT move-view
riverctl map-pointer normal Super BTN_MIDDLE toggle-float
riverctl map-pointer normal Super BTN_RIGHT resize-view

# Rules
riverctl rule-add -title float "float"

# Autostart
autostart="mako"
for program in $autostart; do
	pidof -sx "$program" || riverctl spawn "$program" &
done

riverctl spawn "$XDG_CONFIG_HOME/river/status"
riverctl spawn "$XDG_CONFIG_HOME/river/bar"
riverctl spawn "gpg-agent --daemon"

riverctl declare-mode normal
riverctl default-layout rivertile
riverctl default-attach-mode below
rivertile -view-padding 0 -outer-padding 0 &
