c.url.searchengines = {'DEFAULT': 'https://duckduckgo.com/?q={}',
        'c': 'https://duckduckgo.com/?q={}&ia=chat',
        'y': 'https://invidious.nerdvpn.de/search?q={}',
        'tt': 'https://trakt.tv/search?query={}',
        'gh': 'https://github.com/search?q={}',
        'trans': 'https://lingva.ml/auto/uk/{}'
        }

c.confirm_quit = ['downloads']
c.content.autoplay = False
c.content.blocking.enabled = True
c.content.geolocation = False
c.content.mouse_lock = False
c.content.notifications.enabled = False
c.content.tls.certificate_errors = "block"
c.downloads.location.directory = "$XDG_DOWNLOAD_DIR"
c.downloads.location.prompt = False
c.downloads.remove_finished = 500
c.tabs.background = True
c.url.default_page = 'about:blank'
c.url.start_pages = ['about:blank']
c.qt.args=["enable-accelerated-video-decode", "enable-gpu-rasterization", "ignore-gpu-blocklist"]

black = "#151515"
red = "#f2777a"
green = "#99cc99"
yellow = "#ffcc66"
blue = "#005577"
magenta = "#cc99cc"
white ="#f2f0ec"

c.colors.completion.category.bg = black
c.colors.completion.category.border.bottom = black
c.colors.completion.category.border.top = black
c.colors.completion.category.fg = yellow
c.colors.completion.even.bg = black
c.colors.completion.odd.bg = black
c.colors.completion.scrollbar.bg = black
c.colors.hints.bg = yellow
c.colors.hints.match.fg = yellow
c.colors.messages.error.fg = c.colors.statusbar.normal.fg
c.colors.messages.warning.fg = c.colors.statusbar.normal.fg
c.colors.prompts.fg = c.colors.statusbar.normal.fg
c.colors.statusbar.caret.selection.bg = magenta
c.colors.statusbar.command.bg = black
c.colors.statusbar.normal.bg = black
c.colors.statusbar.url.success.https.fg = white
c.colors.tabs.even.bg = "#002a3b"
c.colors.tabs.even.fg = "#888"
c.colors.tabs.odd.bg = "#002a3b"
c.colors.tabs.odd.fg = "#888"
c.colors.tabs.selected.even.bg = blue
c.colors.tabs.selected.odd.bg = blue
c.colors.webpage.preferred_color_scheme = 'dark'

c.completion.cmd_history_max_items = 10
c.completion.height = "30%"
c.completion.scrollbar.width = 12
c.fonts.default_family = "monospace"
c.fonts.default_size = "8pt"
c.tabs.favicons.scale = 0.9
c.tabs.padding = {"bottom": 2, "left": 5, "right": 5, "top": 2}

config.bind('pd', 'spawn linkhandler {url}')
config.bind('yd', 'hint links spawn linkhandler {hint-url}')

config.load_autoconfig(False)
