return {
   "echasnovski/mini.jump",
   version = "*",
	event = "VeryLazy",
	opts = {
		mappings = {
			forward = "f",
			backward = "F",
			forward_till = "t",
			backward_till = "T",
			repeat_jump = "",
		}
   },
   config = function(_, opts)
      require("mini.jump").setup(opts)
   end,
}
