return {
   "RRethy/base16-nvim",
   lazy = false,
   priority = 1000,
   config = function()
      vim.cmd("colorscheme base16-eighties")

      local hi = vim.cmd.highlight

      local base16 = require("base16-colorscheme")

      base16.setup {
         base00 = '#151515', base01 = '#232323', base02 = '#515151', base03 = '#747369',
         base04 = '#a09f93', base05 = '#d3d0c8', base06 = '#e8e6df', base07 = '#f2f0ec',
         base08 = '#f2777a', base09 = '#f99157', base0A = '#ffcc66', base0B = '#99cc99',
         base0C = '#66cccc', base0D = '#538cc6', base0E = '#cc99cc', base0F = '#d27b53'
      }

      hi({"MiniJump",    "guifg=" .. base16.colors.base0F})
      hi({"LineNr",      "guifg=" .. base16.colors.base03})
      hi({"NormalFloat", "guibg=" .. base16.colors.base01})
      hi({"VertSplit",   "guibg=none", "guifg=" .. base16.colors.base02})
      hi({"StatusLine",  "guibg=" .. base16.colors.base01, "guifg=none"})

      hi({"TelescopeBorder",       "guifg=" .. base16.colors.base02, "guibg=none"})
      hi({"TelescopeMatching",     "guibg=" .. base16.colors.base09, "guifg=" .. base16.colors.base01})
      hi({"TelescopeNormal",       "guifg=" .. base16.colors.base0D, "guibg=none"})
      hi({"TelescopePreviewLine",  "guibg=" .. base16.colors.base02})
      hi({"TelescopePreviewTitle", "guifg=" .. base16.colors.base05, "guibg=none"})
      hi({"TelescopePromptBorder", "guibg=none"})
      hi({"TelescopePromptNormal", "guibg=" .. base16.colors.base01})
      hi({"TelescopePromptTitle",  "guifg=" .. base16.colors.base05, "guibg=none"})
      hi({"TelescopeResultsTitle", "guifg=" .. base16.colors.base05, "guibg=none"})
      hi({"TelescopeSelection",    "guifg=" .. base16.colors.base0A, "guibg=none", "gui=bold"})

      hi({"LspInlayHint",      "gui=none"})
      hi({"TSConstBuiltin",    "gui=none"})
      hi({"TSFuncBuiltin",     "gui=none"})
      hi({"TSTypeBuiltin",     "gui=none"})
      hi({"TSVariableBuiltin", "gui=none"})
      hi({"TreesitterContext", "gui=none"})
   end,
}
