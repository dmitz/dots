return {
   "nvim-telescope/telescope.nvim",
   dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope-project.nvim"
   },
   config = function()
      local telescope = require("telescope")
      local actions = require("telescope.actions")
      local builtin = require("telescope.builtin")
      local extensions = telescope.extensions

      map("n", "<leader>fd", builtin.find_files)
      map("n", "<leader>fb", builtin.buffers)
      map("n", "<leader>fe", builtin.diagnostics)
      map("n", "<leader>fg", builtin.live_grep)
      map("n", "<leader>fh", builtin.help_tags)
      map("n", "<leader>fl", builtin.grep_string)
      map("n", "<leader>fo", builtin.oldfiles)
      map("n", "<leader>gb", builtin.git_branches)
      map("n", "<leader>r",  builtin.resume)
      map("n", "<leader>fp", extensions.project.project)

      telescope.setup {
         defaults = {
            mappings = {
               n, i = {
                  ["<S-CR>"] = actions.select_vertical,
               },
            },
            initial_mode = "normal",
            prompt_prefix = "",
            results_title = false,
            selection_caret = "  ",
            sorting_strategy = "ascending",
            layout_config = {
               horizontal = {
                  prompt_position = "top",
                  width = { padding = 0 },
                  height = { padding = 0 },
                  preview_width = 0.5,
               }
            }
         },
         pickers = {
            buffers = {
               ignore_current_buffer = true,
               sort_lastused = true,
            },
            find_files = {
               hidden = false,
               initial_mode = "insert",
               previewer = false,
            },
            live_grep = {
               initial_mode = "insert",
               no_ignore = true,
            },
            help_tags = {
               initial_mode = "insert"
            }
         }
      }
   end
}
