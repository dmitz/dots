return {
   "lukas-reineke/indent-blankline.nvim",
   main = "ibl",
   config = function()
      local hooks = require("ibl.hooks")
      hooks.register(hooks.type.WHITESPACE, hooks.builtin.hide_first_tab_indent_level)
      hooks.register(hooks.type.WHITESPACE, hooks.builtin.hide_first_space_indent_level)
      require("ibl").setup {
         indent = { char = "┆" },
         whitespace = {
            remove_blankline_trail = false,
         },
         scope = {
            show_start = false,
            show_end = false,
            exclude = { language = { "help" } }
         }
      }
   end
}
