return {
   "neovim/nvim-lspconfig",
   dependencies = {
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-cmdline",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/nvim-cmp",
      "williamboman/mason-lspconfig.nvim",
      "williamboman/mason.nvim",
   },
   config = function()
      local capabilities = vim.lsp.protocol.make_client_capabilities()
      capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

      require("mason").setup()
      require("mason-lspconfig").setup {
         ensure_installed = {
            "csharp_ls",
            "gopls",
         },
         handlers = {
            function(server_name)
               require("lspconfig")[server_name].setup { capabilities = capabilities }
            end
         }
      }

      vim.api.nvim_create_autocmd("LspAttach", {
         group = vim.api.nvim_create_augroup("lsp-attach", { clear = true }),
         callback = function(event)
            local builtin = require("telescope.builtin")
            local map = function(keys, func)
               vim.keymap.set("n", keys, func, { buffer = event.buf })
            end

            map("<leader>D", builtin.lsp_type_definitions)
            map("<leader>ca", vim.lsp.buf.code_action)
            map("<leader>cw", vim.lsp.buf.rename)
            map("<leader>ds", builtin.lsp_document_symbols)
            map("<leader>ws", builtin.lsp_dynamic_workspace_symbols)
            map("gD", vim.lsp.buf.declaration)
            map("gd", builtin.lsp_definitions)
            map("gi", builtin.lsp_implementations)
            map("gr", builtin.lsp_references)
            map("K", vim.lsp.buf.hover)
         end,
      })
   end
}
