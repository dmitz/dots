return {
   "nvim-treesitter/nvim-treesitter",
   dependencies = {
      "nvim-treesitter/nvim-treesitter-textobjects",
      "nvim-treesitter/nvim-treesitter-context",
   },
   build = ":TSUpdate",
   config = function ()
      local configs = require("nvim-treesitter.configs")

      configs.setup({
         sync_install = false,
         auto_install = true,
         highlight = { enable = true },
         indent = { enable = false },
         incremental_selection = {
            enable = true,
            keymaps = {
               node_incremental = "v",
               node_decremental = "b"
            }
         },
         textobjects = {
            select = {
               enable = true,
               keymaps = {
                  ["aa"] = "@parameter.outer",
                  ["ia"] = "@parameter.inner",
                  ["af"] = "@function.outer",
                  ["if"] = "@function.inner",
                  ["ac"] = "@class.outer",
                  ["ic"] = "@class.inner",
                  ["ii"] = "@conditional.inner",
                  ["ai"] = "@conditional.outer",
                  ["il"] = "@loop.inner",
                  ["al"] = "@loop.outer"
               },
               move = {
                  enable = true,
                  goto_next_start = {
                     ["]m"] = "@function.outer",
                     ["]c"] = "@class.outer"
                  },
                  goto_previous_start = {
                     ["[m"] = "@function.outer",
                     ["[c"] = "@class.outer"
                  }
               }
            }
         }
      })
   end,
}
