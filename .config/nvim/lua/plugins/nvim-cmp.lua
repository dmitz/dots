return {
   "hrsh7th/nvim-cmp",
   event = "InsertEnter",
   config = function()
      local cmp = require("cmp")
      cmp.setup {
         preselect = cmp.PreselectMode.None,
         view = {
            entries = { follow_cursor = true }
         },
         mapping = {
            ["<Tab>"] = cmp.mapping.select_next_item(),
            ["<S-Tab>"] = cmp.mapping.select_prev_item(),
            ["<C-d>"] = cmp.mapping.scroll_docs(-4),
            ["<C-u>"] = cmp.mapping.scroll_docs(4),
         },
         sources = {
            {name = "nvim_lsp", max_item_count = 10},
            {name = "buffer", max_item_count = 10},
         }
      }
   end
}

