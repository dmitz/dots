return {
   "nvim-lualine/lualine.nvim",
   opts = {
      options = {
         icons_enabled = false,
         component_separators = { left = "", right = ""},
         section_separators = { left = "", right = ""},
      },
      extensions = {"quickfix", "oil"},
      sections = {
         lualine_a = {
            { "buffers", use_mode_colors = true }
         },
         lualine_b = {},
         lualine_c = {},
         lualine_x = { "location" },
         lualine_y = { { "branch", color = { bg = "#ff9e64" } } },
         lualine_z = {},
      }
   }
}
