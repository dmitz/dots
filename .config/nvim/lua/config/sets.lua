local au = vim.api.nvim_create_autocmd
local bo = vim.bo
local o = vim.o
local wo = vim.wo

o.clipboard = "unnamedplus"
o.completeopt = "menu,menuone,noselect"

o.hlsearch = false
o.incsearch = true
o.ignorecase = true
o.showmode = false

o.mouse = "a"

o.number = true
o.relativenumber = true
o.scrolloff = 8

o.tabstop = 4
o.softtabstop = 4
o.shiftwidth = 4
o.smartindent = true

o.smartcase = true
o.splitright = true

o.swapfile = false
o.undofile = true

o.termguicolors = true
wo.wrap = false

au("TextYankPost", { pattern = "*", callback = function() vim.highlight.on_yank({ higroup="Visual", timeout=150 }) end })
au("BufWritePre", { pattern = "*", command = [[%s/\s\+$//e]] })
au("FileType", { pattern = "help", command = ":wincmd L | :vert" })
au("FileType", { pattern = "lua", command = "setlocal ts=3 sw=3 et" })
au("FileType", { pattern = "yaml", command = "setlocal ts=2 sw=2 et" })
au("FileType", { pattern = "yaml", command = "setlocal ts=2 sw=2 et" })
au("FileType", { pattern = "yaml", command = ":lua vim.b.editorconfig = false" })
