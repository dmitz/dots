map = vim.keymap.set
silent = { silent = true }
vim.g.mapleader = " "

for i = 1, 9 do
   map("n", string.format("<A-%d>", i), string.format(":LualineBuffersJump! %d <cr>", i), silent)
end

map("n", "<C-j>", "<C-w>w")
map("n", "<C-k>", "<C-w>W" )
map("n", "<C-h>", "<c-w>5>")
map("n", "<C-l>", "<c-w>5<" )

map("v", "J", ":m '>+1<cr>gv=gv", silent)
map("v", "K", ":m '<-2<cr>gv=gv", silent)

map("n", "n", "nzzzv")
map("n", "N", "Nzzzv")

map("n", "<C-d>", "<C-d>zz")
map("n", "<C-u>", "<C-u>zz")

map("n", "H", ":bp<cr>", silent)
map("n", "L", ":bn<cr>", silent)
map("n", "X", ":bd<CR>", silent)
map("n", "<A-t>", "<C-^>", silent)
map("n", "<A-x>", ":bd!<CR>", silent)

map("n", "<leader>c",  ":w! | !compiler '<c-r>%'<cr>")
map("n", "<leader>op", ":!opout <c-r>%<cr><cr>")
map("n", "<leader>se", ":setlocal spell! spelllang=en_us<cr>")

map("n", "ge", vim.diagnostic.open_float)
map("n", "[d", vim.diagnostic.goto_prev)
map("n", "]d", vim.diagnostic.goto_next)
