local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("config/sets")
require("config/maps")
require("lazy").setup({
   spec = "plugins",
   change_detection = { notify = false },
   ui = {
      icons = vim.g.have_nerd_font and {} or {
         cmd = "⌘",
         config = "🛠",
         event = "📅",
         ft = "📂",
         init = "⚙",
         keys = "🗝",
         plugin = "🔌",
         runtime = "💻",
         require = "🌙",
         source = "📄",
         start = "🚀",
         task = "📌",
         lazy = "💤 ",
      },
   },
})
